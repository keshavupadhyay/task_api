# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: This api contains Todo managment for user as well as user registratoin with password validation verify for register
* Version: 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

API

#Users#

/api/users/ (User registration endpoint)
/api/users/login/ (User login endpoint)
/api/users/logout/ (User logout endpoint)

#Todos#

/api/todos/ (Todo create and list endpoint)
/api/todos/{todo-id}/ (Todo retrieve, update and destroy endpoint)

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests: No test cases added yet 
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact