from __future__ import unicode_literals

from django.apps import AppConfig


class TodoUserConfig(AppConfig):
    name = 'todo_user'
