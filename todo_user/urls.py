

from django.conf.urls import url
from users.views import UserRegisterView, UserLoginView, UserLogoutView

urlpatterns = [
    url(r'^$', UserRegisterView.as_view(), name="register"),
    url(r'^login/$', UserLoginView.as_view(), name="login"),
    url(r'^logout/$', UserLogoutView.as_view(), name="logout"),
]