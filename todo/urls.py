from django.conf.urls import url
from todos.views import TodoCreateListView, TodoDetailView

urlpatterns = [
    url(r'^$', TodoCreateListView.as_view(), name="todo_list"),
    url(r'^(?P<pk>[0-9]+)/$', TodoDetailView.as_view(), name="detail"),
]