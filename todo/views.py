# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

# Create your views here.

from todos.models import Todo
from todos.serializers import SerializeTodo


class TodoCreateListView(ListCreateAPIView):
    serializer_class = SerializeTodo

    def get_queryset(self):
        return Todo.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class TodoDetailView(RetrieveUpdateDestroyAPIView):
    serializer_class = SerializeTodo
    queryset = Todo.objects.all()
