
from rest_framework import serializers

from django.contrib.auth.models import User


class SerializeTodo(serializers.ModelSerializer):
    user = TodoUserSerializer(read_only=True)

    class Meta:
        model = Todo
        fields = ("user", "name", "done", "date_created")